<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\SalaryDetail;
use App\Http\Requests\SalaryDetailRequest;
use App\Department;
use App\PayrollTemplate;
use App\User;
use Auth;

class SalaryDetailController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
		if(Auth::user()->role->role_permission('view_designation_items')){
			$departments = [''=>''] + Department::lists('department', 'id')->toArray();
			return view('salary-details.index', compact('department', 'departments'));
		}else{
			abort(403);
		}
    }
	
	public function search(Request $request)
	{
		if(Auth::user()->role->role_permission('view_designation_items')){
			$designation_items = DesignationItem::where('designation_item', 'LIKE', '%'. $request->get('term') .'%')->paginate(30);
			return view('designation-items.index', compact('designation_items'));
		}else{
			abort(403);
		}
	}
    
    public function create(Department $department)
    {
		if(Auth::user()->role->role_permission('create_designation_items')){
			$departments = [''=>''] + Department::lists('department', 'id')->toArray();
			return view('designation-items.create', compact('department', 'departments'));
		}else{
			abort(403);
		}
    }
	
    public function store(DesignationItemRequest $request, Department $department)
    {
		if(Auth::user()->role->role_permission('create_designation_items')){
			$designation_item = DesignationItem::create($request->all());
			return redirect(isset($department->id) ? 'departments/'.$department->id : 'designation-items')->withSuccess($designation_item->designation_item.' has been saved.');
		}else{
			abort(403);
		}
    }
	
    public function show(DesignationItem $designation_item)
    {
		if(Auth::user()->role->role_permission('view_designation_items')){
			return $designation_item;
		}else{
			abort(403);
		}
    }
	
    public function edit(Department $department, DesignationItem $designation_item)
    {
		if(Auth::user()->role->role_permission('edit_designation_items')){
			$departments = [''=>''] + Department::lists('department', 'id')->toArray();
			return view('designation-items.edit', compact('designation_item', 'departments', 'department'));
		}else{
			abort(403);
		}
    }
	
    public function update(DesignationItemRequest $request, Department $department, DesignationItem $designation_item)
    {
		if(Auth::user()->role->role_permission('edit_designation_items')){
			$designation_item->update($request->all());
			return redirect(isset($department->id) ? 'departments/'.$department->id : 'designation-items')->withSuccess($designation_item->designation_item.' has been updated.');
		}else{
			abort(403);
		}
    }
	
    public function destroy(Department $department, DesignationItem $designation_item)
    {
		if(Auth::user()->role->role_permission('delete_designation_items')){
			$designation_item->delete();
			return redirect(isset($department->id) ? 'departments/'.$department->id : 'designation-items')->withSuccess($designation_item->designation_item.' has been deleted.');
		}else{
			abort(403);
		}
    }
    
    public function get_employees(Request $request)
    {
        $users = User::where('department_id', $request->get('department_id'))->paginate(30);
        $payroll_templates = [''=>'Select Template'] + PayrollTemplate::lists('hourly_grade', 'id')->toArray();
        return view('salary-details.listing', compact('users','payroll_templates'));
    }
    
    public function save_salary_details(Request $request)
    {
        $employee_id = $request->get('user_id');
        $hourly_rate_id = $request->get('hourly_grade');
        //$payroll_id = $this->input->post('payroll_id', TRUE);
        foreach ($employee_id as $key => $v_emp_id) {
            $user_id = $v_emp_id;
            $hourly_id = $hourly_rate_id[$key];
            if(!empty($hourly_id)){
                DB::table('employee_payroll')->insert(
                    ['user_id' => $user_id, 'hourly_rate_id' => $hourly_id]
                );
            }
        }
        return redirect('salary-details')->withSuccess('Salary Details has been saved.');
    }
}
