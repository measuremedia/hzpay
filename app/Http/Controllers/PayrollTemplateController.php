<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PayrollTemplate;
use App\Http\Requests\PayrollTemplateRequest;
use Auth;

class PayrollTemplateController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index()
    {
        if(Auth::user()->role->role_permission('view_payroll_templates')){
            $payroll_templates = PayrollTemplate::paginate(30);
            return view('payroll-templates.index', compact('payroll_templates'));
        }else{
            abort(403);
        }
    }
	
	public function search(Request $request)
    {
        if(Auth::user()->role->role_permission('view_payroll_templates')){
            $payroll_templates = PayrollTemplate::where('hourly_grade', 'LIKE', '%'. $request->get('term') .'%')->paginate(30);
            return view('payroll-templates.index', compact('payroll_templates'));
        }else{
            abort(403);
        }
    }
    
    public function create()
    {
        if(Auth::user()->role->role_permission('create_payroll_templates')){
            return view('payroll-templates.create');
        }else{
            abort(403);
        }
    }
	
    public function store(PayrollTemplateRequest $request)
    {
        if(Auth::user()->role->role_permission('create_payroll_templates')){
            $payroll_template = PayrollTemplate::create($request->all());
            return redirect('payroll-templates')->withSuccess('Payroll Template has been saved.');
        }else{
            abort(403);
        }
    }
    
    public function show(PayrollTemplate $payroll_template)
    {
        if(Auth::user()->role->role_permission('view_payroll_templates')){
            return $payroll_template;
        }else{
            abort(403);
        }
    }
    
    public function edit(PayrollTemplate $payroll_template)
    {
        if(Auth::user()->role->role_permission('edit_payroll_templates')){
            return view('payroll-templates.edit', compact('payroll_template'));
        }else{
            abort(403);
        }
    }
    
    public function update(PayrollTemplateRequest $request, PayrollTemplate $payroll_template)
    {
        if(Auth::user()->role->role_permission('edit_payroll_templates')){
            $payroll_template->update($request->all());
            return redirect('payroll-templates')->withSuccess('Payroll Template has been updated.');
        }else{
            abort(403);
        }
    }
    
    public function destroy(PayrollTemplate $payroll_template)
    {
        if(Auth::user()->role->role_permission('delete_payroll_templates')){
            $payroll_template->delete();
            return redirect('payroll-templates')->withSuccess('Payroll Template has been deleted.');
        }else{
            abort(403);
        }
    }
}
