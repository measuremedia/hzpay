<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PayrollTemplateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'hourly_grade' => 'required|min:2|unique:payroll_templates,hourly_grade',
                    'hourly_rate' => 'required',
                    'overtime_hours' => 'required'
                ];
            }
            case 'PATCH':
            {
                return [
                    'hourly_grade' => 'required|min:2|unique:payroll_templates,hourly_grade,'.$this->get('id')
                ];
            }
        }
    }
}
