@extends(Auth::check() && Auth::user()->role->layout == 1 ? 'layouts.admin' : 'layouts.employee')

@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>
		Payroll Management
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				@include('alert.success')
				@if(Auth::user()->role->role_permission('create_payroll_templates'))
				<a href="{{ asset('/payroll-templates/create') }}" type="button" class="btn btn-success btn-flat margin">New Payroll Template</a>
				@endif
				<div class="box box-success">
					<div class="box-header">
						<h3 class="box-title">List of Employees and Their Salaries</h3>
						<div class="box-tools">
							{!! Form::open(['url' => '/payroll-templates/search', 'method' => 'get']) !!}
							<div class="input-group input-group-sm" style="width: 150px;">
							{!! Form::text('term', null, ['class' => 'form-control pull-right', 'placeholder' => 'Search']) !!}
							<div class="input-group-btn">
							<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<th>Employee Name</th>
                                <th>Salary Type</th>
								@if(Auth::user()->role->role_permission('edit_payroll_templates') || Auth::user()->role->role_permission('delete_payroll_templates'))
								<th>Action</th>
								@endif
							</tr>
							@foreach($users as $user)
							<tr>
								<td>{{ $user->first_name}}</td>
                                <td>{{ $user->hourly_grade }}</td>
								@if(Auth::user()->role->role_permission('edit_payroll_templates') || Auth::user()->role->role_permission('delete_payroll_templates'))
								<td>
									<div class="btn-group">
										@if(Auth::user()->role->role_permission('edit_payroll_templates'))
										<a href="{{ asset('salary-list/'.$user->id.'/edit') }}" title="Edit" class="btn btn-default btn-flat btn-sm"><i class="fa fa-pencil"></i></a>
										@endif
										@if(Auth::user()->role->role_permission('delete_payroll_templates'))
										<a type="button" class="btn btn-default btn-flat btn-sm" title="Delete" data-toggle="modal" data-target="#deleteModal{{ $user->id }}"><i class="fa fa-trash"></i></a>
										<div id="deleteModal{{ $user->id }}" class="modal fade" role="dialog">
											<div class="modal-dialog">
											<div class="modal-content">
											<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Confirm Delete</h4>
											</div>
											<div class="modal-body">
											<p>Are you sure you want to delete this item?</p>
											</div>
											<div class="modal-footer">
											<button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Close</button>
											{!! Form::open(['url' => ['payroll-templates/'.$user->id], 'method' => 'delete']) !!}
											{!! Form::submit('Yes', ['class' => 'btn btn-success btn-flat']) !!}
											{!! Form::close() !!}
											</div>
											</div>
											</div>
										</div>
										@endif
									</div>
								</td>
								@endif
							</tr>
							@endforeach
						</table>
					</div>
					
				</div>
			</div>
		</div>
	</section>
</div>
@endsection