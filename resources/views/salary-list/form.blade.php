<div class="form-group{{ $errors->has('payroll_template') ? ' has-error' : '' }}">
	{!! Form::label('hourly_grade', 'Template Name') !!}
    {!! Form::text('hourly_grade', null, ['class' => 'form-control']) !!}
    @if ($errors->has('hourly_grade'))
        <span class="help-block">
            <strong>{{ $errors->first('hourly_grade') }}</strong>
        </span>
    @endif
    {!! Form::label('hourly_rate', 'Payroll Template') !!}
    {!! Form::text('hourly_rate', null, ['class' => 'form-control']) !!}
    @if ($errors->has('hourly_rate'))
        <span class="help-block">
            <strong>{{ $errors->first('hourly_rate') }}</strong>
        </span>
    @endif
    {!! Form::label('overtime_hours', 'Overtime Rate') !!}
    {!! Form::text('overtime_hours', null, ['class' => 'form-control']) !!}
    @if ($errors->has('overtime_hours'))
        <span class="help-block">
            <strong>{{ $errors->first('overtime_hours') }}</strong>
        </span>
    @endif
</div>
<div class="pull-right">
	{!! Form::submit('Save', ['class' => 'btn btn-success btn-flat']) !!}
	<a href="{{ asset('/payroll-templates') }}" type="button" class="btn btn-default btn-flat">Cancel</a>
</div>