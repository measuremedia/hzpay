@if(isset($department) && $department->count() == 0)
<div class="form-group">
	{!! Form::label('department_id', 'Select Department') !!}
	{!! Form::text('department', $department->department, ['class' => 'form-control', 'disabled' => '']) !!}
	{!! Form::hidden('department_id', $department->id) !!}
</div>
@else
<div class="form-group{{ $errors->has('department_id') || $errors->has('department') ? ' has-error' : '' }}">
	{!! Form::label('department_id', 'Select Department') !!}<br />
	{!! Form::select('department_id', $departments, null, ['class' => 'form-control']) !!}
	@if ($errors->has('department_id') || $errors->has('department'))
		<span class="help-block">
			<strong>{{ $errors->has('department') ? $errors->first('department') : $errors->first('department_id') }}</strong>
		</span>
	@endif
</div>
@endif
<div class="pull-right">
	{!! Form::submit('Go', ['class' => 'btn btn-success btn-flat']) !!}
</div>