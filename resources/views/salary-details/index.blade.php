@extends(Auth::check() && Auth::user()->role->layout == 1 ? 'layouts.admin' : 'layouts.employee')
@section('head')
    <link href="{{ asset('/plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endsection
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
        Payroll Management
        </h1>
    </section>

    <section class="content">
        <div class="row">
        @include('alert.success')
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-success">
                    <div class="box-header">    
                        <h3 class="box-title">Manage Salary Details</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['id'=>'salary-detail','url' => isset($department->id) ? 'departments/'.$department->id.'/salary-details' : 'salary-details/get_employees']) !!}
                        @include('salary-details.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div id="campaign">
        </div> 
    </section>
</div>
@endsection

@section('foot')
    <script src="{{ asset('/plugins/select2/select2.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $("select").select2({
                placeholder: "Select",
                allowClear: true
            });
        });
        $(function(){
        $('#salary-detail').on('submit',function(e){
            $.ajaxSetup({
                header:$('meta[name="_token"]').attr('content')
            })
            e.preventDefault(e);

                $.ajax({

                type:"POST",
                url:'salary-details/get_employees',
                data:$(this).serialize(),
                success: function(data){
                    console.log(data);
                    $("#campaign").html(data);
                },
                error: function(data){

                }
            })
            });
        });
        /*function displayVals(data)
        {
            var val = data;
            $.ajax({
            type: "POST",
            url: "get_employees",
            data: { id : val },
                success:function(employees)
                {
                    $("#campaign").html(employees);
                }
            });
        }*/
    </script>
@endsection
