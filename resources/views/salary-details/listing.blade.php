
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				@include('alert.success')
                {!! Form::open(['id'=>'set-template','url' => 'salary-details/save_salary_details']) !!}
				<div class="box box-success">
					<div class="box-header">
						<h3 class="box-title">Employee List</h3>
					</div>
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tr>
								<th>Employee Name</th>
                                <th>Designation</th>
								<th>Payroll Template</th>
							</tr>
							@foreach($users as $user)
							<tr>
								<td>{{ $user->first_name.' '.$user->last_name }}({{ $user->role ? $user->role->role : '' }})
                                    {!! Form::hidden('user_id[]', $user->id) !!}
                                </td>								
								<td>{{ $user->designation_item ? $user->designation_item->designation_item : '' }}</td>
                                <td>{!! Form::select('hourly_grade[]', $payroll_templates, null, ['class' => 'form-control']) !!}</td>
							</tr>
							@endforeach
						</table>
					</div>
					<div class="box-footer clearfix">
						<div class="row">
							<div class="col-xs-12">
								Showing {{ $users->firstItem() }} to {{ $users->lastItem() }} of {{ $users->total() }} entries
								<div class="pull-right">
								{!! $users->links() !!}
								</div>
							</div>
						</div>
					</div>
				</div>
                <div class="pull-right">
                    {!! Form::submit('Update', ['class' => 'btn btn-success btn-flat']) !!}
                </div>
                {!! Form::close() !!}
			</div>
		</div>
	</section>

